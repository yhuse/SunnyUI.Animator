﻿# SunnyUI.Animator    
    
#### 介绍    
Animator 组件允许您为 C# WinForms 上的任何控件设置动画。    
原项目地址： [http://www.codeproject.com/Articles/548769/Animator-for-WinForms](http://www.codeproject.com/Articles/548769/Animator-for-WinForms)    
原项目更新日志：    
19 Feb 2013 - First release.    
20 Feb 2013 - TabControlEx was added, ClipRectangle is available for Update mode.    
22 Feb 2013 - Performance was increased. Some bugs was fixed. Decoration sample was added.     
    
修改内容：          
V1.0.3 2022-7-27      
1、增加向右滑动动画    
2、增加向下卷动动画    
3、升级.Net版本，支持.Net Framework 4.0~4.8、.Net 6   
4、增加帮助类，不用在界面上放置Animator控件，详见 Tester/SimplestSample.cs    
~~~
//设置动画，执行，等待执行完成
    AnimatorHelper.Instance.SetAnimationType(AnimationType.HorizSlide).Hide(pb1);
    AnimatorHelper.Instance.WaitAllAnimations();

    AnimatorHelper.Instance.SetAnimationType(AnimationType.HorizRevSlide).Show(pb1);
    AnimatorHelper.Instance.WaitAllAnimations();

//设置多个动画叠加，执行，等待执行完成
    AnimatorHelper.Instance.SetAnimation(Animation.HorizRevSlide, Animation.VertRevSlide).Show(pb1);
    AnimatorHelper.Instance.WaitAllAnimations();
~~~
  
    
#### 原项目Readme    
The component Animator allows you to animate any controls on your WinForms.    
    
Animator    
========    
    
The component Animator allows you to animate any controls on your WinForms.    
    
<img src="http://www.codeproject.com/KB/miscctrl/548769/Project010_c.gif">    
<img src="http://www.codeproject.com/KB/miscctrl/548769/Project011.gif">    
<img src="http://www.codeproject.com/KB/miscctrl/548769/Project012.gif">    
        
More details: [http://www.codeproject.com/Articles/548769/Animator-for-WinForms](http://www.codeproject.com/Articles/548769/Animator-for-WinForms)    
