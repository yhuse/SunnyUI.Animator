﻿using AnimatorNS;
using Sunny.UI;
using System;
using System.Windows.Forms;

namespace Tester
{
    public partial class SimplestSample : Form
    {
        public SimplestSample()
        {
            InitializeComponent();
        }

        private void btHide_Click(object sender, EventArgs e)
        {
            animator.Hide(pb1);
        }

        private void btShow_Click(object sender, EventArgs e)
        {
            animator.Show(pb1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int idx = random.Next((int)AnimationType.VertRevSlide);
            AnimationType type = (AnimationType)(idx++);
            AnimatorHelper.Instance.SetAnimationType(type).Hide(pb1);
            AnimatorHelper.Instance.WaitAllAnimations();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int idx = random.Next((int)AnimationType.VertRevSlide);
            AnimationType type = (AnimationType)(idx++);
            AnimatorHelper.Instance.SetAnimationType(type).Show(pb1);
            AnimatorHelper.Instance.WaitAllAnimations();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            AnimatorHelper.Instance.SetAnimationType(AnimationType.HorizSlide).Hide(pb1);
            AnimatorHelper.Instance.WaitAllAnimations();

            AnimatorHelper.Instance.SetAnimationType(AnimationType.HorizRevSlide).Show(pb1);
            AnimatorHelper.Instance.WaitAllAnimations();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AnimatorHelper.Instance.SetAnimationType(AnimationType.VertSlide).Hide(pb1);
            AnimatorHelper.Instance.WaitAllAnimations();

            AnimatorHelper.Instance.SetAnimationType(AnimationType.VertRevSlide).Show(pb1);
            AnimatorHelper.Instance.WaitAllAnimations();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            AnimatorHelper.Instance.SetAnimationType(AnimationType.HorizBlind).Hide(pb1);
            AnimatorHelper.Instance.WaitAllAnimations();

            AnimatorHelper.Instance.SetAnimationType(AnimationType.HorizRevBlind).Show(pb1);
            AnimatorHelper.Instance.WaitAllAnimations();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            AnimatorHelper.Instance.SetAnimationType(AnimationType.VertBlind).Hide(pb1);
            AnimatorHelper.Instance.WaitAllAnimations();

            AnimatorHelper.Instance.SetAnimationType(AnimationType.VertRevBlind).Show(pb1);
            AnimatorHelper.Instance.WaitAllAnimations();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            AnimatorHelper.Instance.SetAnimation(Animation.HorizSlide, Animation.VertSlide).Hide(pb1);
            AnimatorHelper.Instance.WaitAllAnimations();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            AnimatorHelper.Instance.SetAnimation(Animation.HorizRevSlide, Animation.VertRevSlide).Show(pb1);
            AnimatorHelper.Instance.WaitAllAnimations();
        }
    }
}
