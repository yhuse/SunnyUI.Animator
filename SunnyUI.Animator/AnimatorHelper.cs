﻿using AnimatorNS;

namespace Sunny.UI
{
    public static class AnimatorHelper
    {
        public static Animator Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Animator();
                }

                return instance;
            }
        }

        private static Animator instance;

        public static Animator SetAnimationType(this Animator animator, AnimationType animationType)
        {
            if (animator == null) return animator;
            animator.AnimationType = animationType;
            return animator;
        }

        public static Animator SetAnimation(this Animator animator, params Animation[] animations)
        {
            if (animator == null) return animator;
            if (animations == null || animations.Length == 0) return animator;

            Animation a = animations[0];
            if (animations.Length > 1)
            {
                for (int i = 1; i < animations.Length; i++)
                {
                    a.Add(animations[i]);
                }
            }

            animator.DefaultAnimation = a;
            return animator;
        }
    }
}
